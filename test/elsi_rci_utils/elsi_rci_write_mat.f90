! Copyright (c) 2015-2019, the ELSI team.
! All rights reserved.
!
! This file is part of ELSI and is distributed under the BSD 3-clause license,
! which may be found in the LICENSE file in the ELSI root directory.

!>
!! This module contains subroutines to write matrices.
!!
module ELSI_RCI_WRITE_MAT

    use ELSI_RCI_PRECISION, only: r8,i4,i8

    implicit none

    private

    public :: rci_write_mat_real
    public :: rci_write_vec_real
    public :: rci_write_mat_cmplx
    public :: rci_write_vec_cmplx

contains

subroutine rci_write_mat_real(matfile,mat)

    implicit none

    character(*), intent(in) :: matfile
    real(r8), intent(in):: mat(:,:)

    integer(i4) :: m,n
    integer(i4) :: i_row
    integer(i4) :: i_col


    ! Open file
    open(file=matfile,unit=30,action='write')

    m = size(mat,1)
    n = size(mat,2)

    do i_row = 1, m
        do i_col = 1, n
            write (30,'(F32.16) ',advance='no') mat(i_row,i_col)
        enddo
        write (30,*) ''
    enddo

    ! Close file
    close(unit=30)

end subroutine

subroutine rci_write_vec_real(vecfile,vec)

    implicit none

    character(*), intent(in) :: vecfile
    real(r8), intent(in):: vec(:)

    integer(i4) :: m
    integer(i4) :: i_row


    ! Open file
    open(file=vecfile,unit=30,action='write')

    m = size(vec)

    do i_row = 1, m
        write (30,'(F32.16) ') vec(i_row)
    enddo

    ! Close file
    close(unit=30)

end subroutine

subroutine rci_write_mat_cmplx(matfile,mat)

    implicit none

    character(*), intent(in) :: matfile
    complex(r8), intent(in):: mat(:,:)

    integer(i4) :: m,n
    integer(i4) :: i_row
    integer(i4) :: i_col


    ! Open file
    open(file=matfile,unit=30,action='write')

    m = size(mat,1)
    n = size(mat,2)

    do i_row = 1, m
        do i_col = 1, n
            write (30,'(A, F32.16, A, F32.16, A) ',advance='no') &
                'complex(', real(mat(i_row,i_col)), &
                ',', imag(mat(i_row,i_col)), ') '
        enddo
        write (30,*) ''
    enddo

    ! Close file
    close(unit=30)

end subroutine

subroutine rci_write_vec_cmplx(vecfile,vec)

    implicit none

    character(*), intent(in) :: vecfile
    complex(r8), intent(in):: vec(:)

    integer(i4) :: m
    integer(i4) :: i_row


    ! Open file
    open(file=vecfile,unit=30,action='write')

    m = size(vec)

    do i_row = 1, m
        write (30,'((F32.16, F32.16)) ',advance='no') vec(i_row)
        write (30,'(A, F32.16, A, F32.16, A) ',advance='no') &
                'complex(', real(vec(i_row)), ',', imag(vec(i_row)), ') '
    enddo

    ! Close file
    close(unit=30)

end subroutine

end module ELSI_RCI_WRITE_MAT
