### ELSI_RCI source files ###
LIST(APPEND elsi_rci_src
  elsi_rci_constants.f90
  elsi_rci_datatype.f90
  elsi_rci_interface.f90
  elsi_rci_c_interface.f90
  elsi_rci_ops.f90
  elsi_rci_precision.f90
  elsi_rci_setup.f90
  Davidson/elsi_rci_davidson.f90
  OMM/elsi_rci_omm.f90
  OMM/rci_omm_cubic.f90
  PPCG/elsi_rci_ppcg.f90
  ChebFilter/elsi_rci_chebfilter.f90)

ADD_LIBRARY(elsi_rci ${elsi_rci_src})


### Generate pkg-config file ###
GET_TARGET_PROPERTY(PKG_LIBS elsi_rci LINK_LIBRARIES)
SET(PKG_LIBS ";elsi_rci;${PKG_LIBS}")
FOREACH(_lib ${PKG_LIBS})
  GET_FILENAME_COMPONENT(_lib_we ${_lib} NAME_WE)
  STRING(REPLACE "${_lib}" "${_lib_we}" PKG_LIBS "${PKG_LIBS}")
ENDFOREACH()
STRING(REPLACE ";lib" ";" PKG_LIBS "${PKG_LIBS}")
STRING(REPLACE ";" " -l" PKG_LIBS "${PKG_LIBS}")
IF(LIB_PATHS)
  SET(PKG_LIB_PATHS ";${LIB_PATHS}")
  STRING(REPLACE ";" " -L" PKG_LIB_PATHS "${PKG_LIB_PATHS}")
ENDIF()
IF(INC_PATHS)
  SET(PKG_INC_PATHS ";${INC_PATHS}")
  STRING(REPLACE ";" " -I" PKG_INC_PATHS "${PKG_INC_PATHS}")
ENDIF()

CONFIGURE_FILE(${PROJECT_SOURCE_DIR}/.elsi_rci.pc.in
  ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/pkgconfig/elsi_rci.pc
  @ONLY)

### Installation ###
# Libraries
INSTALL(TARGETS elsi_rci
  EXPORT elsi_rciConfig
  INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR})
# Header files
INSTALL(DIRECTORY ${CMAKE_Fortran_MODULE_DIRECTORY}/
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
  PATTERN "*.mod"
  PATTERN "mkl*.mod" EXCLUDE)
INSTALL(FILES elsi_rci.h
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
# pkg-config file
INSTALL(FILES ${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/pkgconfig/elsi_rci.pc
  DESTINATION ${CMAKE_INSTALL_LIBDIR}/pkgconfig)
# elsi_rciConfig.cmake
INSTALL(EXPORT elsi_rciConfig
  NAMESPACE elsi_rci::
  DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/elsi_rci)
# elsi_rciConfigVersion.cmake
INCLUDE(CMakePackageConfigHelpers)
WRITE_BASIC_PACKAGE_VERSION_FILE(
  ${PROJECT_BINARY_DIR}/elsi_rciConfigVersion.cmake
  VERSION ${PROJECT_VERSION}
  COMPATIBILITY SameMajorVersion)
INSTALL(FILES ${PROJECT_BINARY_DIR}/elsi_rciConfigVersion.cmake
  DESTINATION ${CMAKE_INSTALL_LIBDIR}/cmake/elsi_rci)
# Export ELSI
TARGET_INCLUDE_DIRECTORIES(elsi_rci INTERFACE
  $<BUILD_INTERFACE:${CMAKE_Fortran_MODULE_DIRECTORY}>)
EXPORT(EXPORT elsi_rciConfig NAMESPACE elsi_rci::
  FILE ${PROJECT_BINARY_DIR}/elsi_rciConfig.cmake)

